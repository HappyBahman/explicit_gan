"""
Usage:
    inception.py [options] <gen_model> <cd3_model>

Options:
    -n, --num_videos=<count>                number of videos to generate [default: 1000]
    -f, --number_of_frames=<count>          generate videos with that many frames [default: 16]

    --load_from_dir                         load videos instead of generating them

    --mean=<path>                           the mean to substract from [default: ./mean2.npz]
    --seed=<int>                            random seed [default: 0]
    --interpolation=<str>                   interpolation method for videos [default: BICUBIC]
    --result_dir=path                       folder in which to write the results [default: ./results]
"""


import os
import time
import argparse
import sys
from tqdm import tqdm
import docopt

import numpy as np
import cv2 as cv

import torch
from torch import nn
from torchvision import transforms
from PIL import Image
from c3d import C3D
sys.path.insert(0, './..')
from trainers import videos_to_numpy
from models import VideoGenerator

activation = {}


def get_activation(name):
    def hook(model, input, output):
        activation[name] = output.detach()
    return hook


def calc_inception(ys):
    N, C = ys.shape
    p_all = np.mean(ys, axis=0, keepdims=True)
    kl = np.sum(ys * np.log(ys + 1e-7) - ys * np.log(p_all + 1e-7)) / N
    return np.exp(kl)


def main():
    args = docdocopt(__doc__)
    print(args)

    np.random.seed(int(args['--seed']))

    inter_method = args['--interpolation']
    interpolation = getattr(Image, args['--interpolation'])

    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')

    c3dmodel = C3D(101, True, args['<cd3_model>'])
    sm = nn.Softmax(dim=1)

    # c3dmodel.load_state_dict(torch.load(args.path, map_location=device))

    # load model
    generator = torch.load(args["<gen_model>"], map_location={'cuda:0': 'cpu'})
    generator.to(device)
    generator.eval()
    num_videos = int(args['--num_videos'])
    #todo save videos?
    result_dir = args['--result_dir']

    if not os.path.exists(result_dir):
        os.makedirs(result_dir)

    mean = np.load(args['--mean'])['mean'].astype('f')
    mean = mean.reshape((3, 1, 16, 128, 171))[:, :, :, :, 21:21 + 128]

    # generator
    ys = []
    batchsize = 50
    my_transforms = transforms.Compose([
        transforms.ToPILImage(mode=None),
        transforms.Resize((128, 128), interpolation=interpolation),
        transforms.ToTensor()
    ])
    for i in tqdm(range(num_videos // batchsize)):
        x, _ = generator.sample_videos(
            batchsize, int(args['--number_of_frames']))

        # x = videos_to_numpy(x).squeeze().transpose((0, 1, 2, 3, 4))
        n, c, f, h, w = x.shape

        x = torch.reshape(x.permute(0, 2, 3, 4, 1), (n * f, h, w, c))
        x = x * 128 + 128
        x_ = torch.zeros((n * f, 3, 128, 128))
        for t in range(n * f):
            x_[t] = my_transforms(x[t].cpu())
        x = torch.reshape(x_.permute(3, 0, 1, 2), (3, n, f, 128, 128))
        x = x - mean[::-1]  # mean file is BGR-order while model outputs RGB-order
        x = x[:, :, :, 8:8 + 112, 8:8 + 112]
        x = x.permute(1, 0, 2, 3, 4)
        with torch.no_grad():
            # C3D takes an image with BGR order
            # todo, get data for prob layer
            c3dmodel.fc8.register_forward_hook(get_activation('fc8'))
            y = c3dmodel(x)
            ys.append(sm(activation['fc8']).numpy())
    # print(ys)
    ys = np.asarray(ys).reshape((-1, 101))
    score = calc_inception(ys)
    with open('{}/inception_iter-{}.txt'.format(result_dir, inter_method), 'w') as fp:
        print(result_dir, score, file=fp)
        print(result_dir, 'score:{}'.format(score))

    return 0


if __name__ == '__main__':
    sys.exit(main())
