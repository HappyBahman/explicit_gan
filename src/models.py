"""
The code for vae is in the
https://github.com/nicktfranklin/VAE-video/blob/master/pytorch_vae.py
"""

import torch
import torch.nn as nn
import torch.nn.parallel
import torch.utils.data
from torch.autograd import Variable

import numpy as np

from torchvision import transforms
from matplotlib import pyplot as plt
import math


if torch.cuda.is_available():
    T = torch.cuda
else:
    T = torch


class Noise(nn.Module):
    def __init__(self, use_noise, sigma=0.2):
        super(Noise, self).__init__()
        self.use_noise = use_noise
        self.sigma = sigma

    def forward(self, x):
        if self.use_noise:
            return x + self.sigma * Variable(T.FloatTensor(x.size()).normal_(), requires_grad=False)
        return x

class Flatten(torch.nn.Module):
    def forward(self, x):
        return x.view(x.size(0), -1)


class Reshape(torch.nn.Module):
    def __init__(self, outer_shape):
        super(Reshape, self).__init__()
        self.outer_shape = outer_shape

    def forward(self, x):
        return x.view(x.size(0), *self.outer_shape)


# Encoder and decoder use the DC-GAN architecture
# Applying lower dimension 3D architecture as in paper:
# ldvd-GAN: Lower Dimensional Kernels for Video Discriminators
# code in https://github.com/HappyBahman/ldvdGAN
class Encoder(torch.nn.Module):
    def __init__(self, z_dim, n_channel=3, size_=3, size_w=2):
        super(Encoder, self).__init__()
        self.model = torch.nn.ModuleList([
            torch.nn.Conv3d(n_channel, 32 * n_channel, (1, 16, 16), (1, 4, 4), padding=(0, 1, 1)),
            torch.nn.Conv3d(32 * n_channel, 32 * n_channel, (4, 1, 1), (2, 1, 1), padding=(1, 0, 0)),
            torch.nn.LeakyReLU(),
            torch.nn.Conv3d(32 * n_channel, 64 * n_channel, (1, 8, 8), (1, 4, 4), padding=(0, 1, 1)),
            torch.nn.Conv3d(64 * n_channel, 64 * n_channel, (4, 1, 1), (2, 1, 1), padding=(1, 0, 0)),
            torch.nn.LeakyReLU(),
            torch.nn.Conv3d(64 * n_channel, 128 * n_channel, (1, 4, 4), (1, 2, 2), padding=(0, 1, 1)),
            torch.nn.Conv3d(128 * n_channel, 128 * n_channel, (4, 1, 1), (2, 1, 1), padding=(1, 0, 0)),
            torch.nn.LeakyReLU(),
            Flatten(),
            torch.nn.Linear(128 * n_channel * (size_ ** 2) * size_w, 512 * n_channel),
            torch.nn.LeakyReLU(),
            torch.nn.Linear(512 * n_channel, z_dim)
        ])

    def forward(self, x):
        # print('Encoder')
        # print(x.size())
        for layer in self.model:
            # print(layer)
            # print(x)
            x = layer(x)
        #     print(x.size())
        # print('===================')
        return x


class Decoder(torch.nn.Module):
    def __init__(self, z_dim, n_channel=3, size_=3, size_w=2):
        super(Decoder, self).__init__()
        self.model = torch.nn.ModuleList([
            torch.nn.Linear(z_dim, 512 * n_channel),
            torch.nn.ReLU(),
            torch.nn.Linear(512 * n_channel, 128 * n_channel * (size_ ** 2) * size_w),
            torch.nn.ReLU(),
            Reshape((128 * n_channel, size_w, size_, size_)),
            torch.nn.ConvTranspose3d(128 * n_channel, 128 * n_channel, (4, 1, 1), (2, 1, 1), padding=(1, 0, 0)),
            torch.nn.ConvTranspose3d(128 * n_channel, 64 * n_channel, (1, 4, 4), (1, 2, 2), padding=(0, 0, 0)),
            torch.nn.ReLU(),
            torch.nn.ConvTranspose3d(64 * n_channel, 64 * n_channel, (4, 1, 1), (2, 1, 1), padding=(1, 0, 0)),
            torch.nn.ConvTranspose3d(64 * n_channel, 32 * n_channel, (1, 8, 8), (1, 4, 4), padding=(0, 1, 1)),
            torch.nn.ReLU(),
            torch.nn.ConvTranspose3d(32 * n_channel, 32 * n_channel, (4, 1, 1), (2, 1, 1), padding=(1, 0, 0)),
            torch.nn.ConvTranspose3d(32 * n_channel, n_channel, (1, 16, 16), (1, 4, 4), padding=(0, 10, 10)),
            torch.nn.Sigmoid()
        ])

    def forward(self, x):
        # print('Decoder')
        # print(x.size())
        for layer in self.model:
            # print(layer)
            x = layer(x)
            # print(x.size())
        return x


class MaskDecoder(torch.nn.Module):
    def __init__(self, z_dim, n_channel=3, size_=8, noise_dim=None):
        if noise_dim is None:
            noise_dim = z_dim
        super(Decoder, self).__init__()
        self.model = torch.nn.ModuleList([
            torch.nn.Linear(z_dim + noise_dim, 1024 * n_channel),
            torch.nn.ReLU(),
            torch.nn.Linear(1024 * n_channel, 256 * (size_ ** 2) * n_channel),
            torch.nn.ReLU(),
            Reshape((256 * n_channel, size_, size_)),
            torch.nn.ConvTranspose3d(256 * n_channel, 256 * n_channel, (4, 1, 1), (2, 1, 1), padding=(1, 0, 0)),
            torch.nn.ConvTranspose3d(256 * n_channel, 128 * n_channel, (1, 4, 4), (1, 2, 2), padding=(0, 1, 1)),
            torch.nn.ReLU(),
            torch.nn.ConvTranspose3d(128 * n_channel, 128 * n_channel, (4, 1, 1), (2, 1, 1), padding=(1, 0, 0)),
            torch.nn.ConvTranspose3d(128 * n_channel, 64 * n_channel, (1, 4, 4), (1, 2, 2), padding=(0, 1, 1)),
            torch.nn.ReLU(),
            torch.nn.ConvTranspose3d(64 * n_channel, 64 * n_channel, (4, 1, 1), (2, 1, 1), padding=(1, 0, 0)),
            torch.nn.ConvTranspose3d(64 * n_channel, n_channel, (1, 4, 4), (1, 2, 2), padding=(0, 1, 1)),
            torch.nn.Sigmoid()
        ])

    def forward(self, x, noise):
        # print('Decoder')
        # print(x.size())
        for layer in self.model:
            input_ = torch.cat((x, noise), 0)
            x = layer(input_)
            # print(x.size())
        return x


def compute_kernel(x, y):
    x_size = x.size(0)
    y_size = y.size(0)
    dim = x.size(1)
    x = x.unsqueeze(1)  # (x_size, 1, dim)
    y = y.unsqueeze(0)  # (1, y_size, dim)
    tiled_x = x.expand(x_size, y_size, dim)
    tiled_y = y.expand(x_size, y_size, dim)
    kernel_input = (tiled_x - tiled_y).pow(2).mean(2) / float(dim)
    return torch.exp(-kernel_input)  # (x_size, y_size)


def compute_mmd(x, y):
    x_kernel = compute_kernel(x, x)
    y_kernel = compute_kernel(y, y)
    xy_kernel = compute_kernel(x, y)
    mmd = x_kernel.mean() + y_kernel.mean() - 2 * xy_kernel.mean()
    return mmd


class l_dim_vae(torch.nn.Module):
    def __init__(self, z_dim, n_channel=3, size_=8):
        super(l_dim_vae, self).__init__()
        self.encoder = Encoder(z_dim, n_channel, size_)
        self.decoder = Decoder(z_dim, n_channel, size_)

    def forward(self, x):
        z = self.encoder(x)
        x_reconstructed = self.decoder(z)
        # print(x.size())
        with torch.no_grad():
            x_max = torch.max(x, dim = 1).values
            # print('=====')
            # print(x.size())
            # print(x_max.size())
            mask_t = torch.ceil(( x_max + 1)/2)
            mask = mask_t.unsqueeze(1).expand(x.size()[0], x.size()[1], -1, -1,  -1)
            # print(mask.size())
            # print(x.size())
        # ones = (mask == 1.).sum()
        # zeros = (mask == 0.).sum()
        # print(torch.numel(mask) - zeros - ones)
        # print(zeros + ones)
        # print(torch.max(mask))
        # print(torch.min(mask))
        x_reconstructed = (x_reconstructed * mask) * 2 - 1
        return z, x_reconstructed, mask


class exp_vae(torch.nn.Module):
    """
        this VAE first learns to reconstruct the empty mask thus learning to
        encode the location info. of each frame
        It then learns to reconstruct the whole image, in a setting where
        locations are encoded.
    """
    def __init__(self, z_dim, n_channel=3, size_=3, batch_size=3):
        super(exp_vae, self).__init__()
        self.z_dim = int(z_dim/2)
        self.mask_encoder = Encoder(int(z_dim/2), n_channel, size_)
        self.obj_encoder = Encoder(int(z_dim/2), n_channel, size_)
        self.decoder = Decoder(z_dim, n_channel, size_)
        self.batch_size = batch_size

    def forward(self, x_mask, x_original=None, masked=False, GAN=False):
        # encode the info for location of objects
        z_location = self.mask_encoder(x_mask)
        # either encode the info for objects or
        # encode nothing (-1)
        if masked:
            z_content = Variable(torch.ones(self.batch_size, self.z_dim).cuda(), requires_grad=False) * -1
        elif GAN:
            z_content = self.sample_z_content(self.z_dim, self.batch_size)
        else:
            assert x_original is not None, "Did not pass an original image or passed a None object"
            z_content = self.mask_encoder(x_original)
        # concat two types of info
        # z_location = z_location.cuda()
        z_content = z_content.cuda()
        z = torch.cat([z_location, z_content], 1)
        # reconstruct the image
        x_reconstructed = self.decoder(z)
        if not masked:
            x_reconstructed = (x_reconstructed * x_mask) * 2 - 1
        return z, x_reconstructed, x_mask

    def sample_z_content(self, z_dim, batch_size):
        return Variable(torch.randn(batch_size, z_dim).cuda(), requires_grad=False)

    def sample_fake_videos(self, batch_size):
        z = self.sample_z_content(self.z_dim, batch_size)
        generated = self.decoder(z)
        return generated

class PatchImageDiscriminator(torch.nn.Module):
    def __init__(self, n_channels, ndf=64, use_noise=False, noise_sigma=None):
        super(PatchImageDiscriminator, self).__init__()

        self.use_noise = use_noise

        self.main = nn.Sequential(
            Noise(use_noise, sigma=noise_sigma),
            nn.Conv2d(n_channels, ndf, 4, 2, 1, bias=False),
            nn.LeakyReLU(0.2, inplace=True),

            Noise(use_noise, sigma=noise_sigma),
            nn.Conv2d(ndf, ndf * 2, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 2),
            nn.LeakyReLU(0.2, inplace=True),

            Noise(use_noise, sigma=noise_sigma),
            nn.Conv2d(ndf * 2, ndf * 4, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 4),
            nn.LeakyReLU(0.2, inplace=True),

            Noise(use_noise, sigma=noise_sigma),
            nn.Conv2d(ndf * 4, 1, 4, 2, 1, bias=False),
        )

    def forward(self, input):
        h = self.main(input).squeeze()
        return h, None


class FactorizedPatchVideoDiscriminator(nn.Module):
    '''
    this function is identical to PatchVideoDiscriminator with the exception that all
    3D convolutions are factorized
    for more details see:
        https://arxiv.org/pdf/1912.08860.pdf
    '''
    def __init__(self, n_channels, n_output_neurons=1, bn_use_gamma=True, use_noise=False, noise_sigma=None, ndf=64):
        super(FactorizedPatchVideoDiscriminator, self).__init__()

        self.n_channels = n_channels
        self.n_output_neurons = n_output_neurons
        self.use_noise = use_noise
        self.bn_use_gamma = bn_use_gamma

        self.model = torch.nn.ModuleList([
            Noise(use_noise, sigma=noise_sigma),
            nn.Conv3d(n_channels, ndf, (1, 4, 4), stride=(1, 2, 2), padding=(0, 1, 1), bias=False),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Conv3d(ndf, ndf, (4, 1, 1), stride=1, padding=(1, 0, 0), bias=False),
            nn.LeakyReLU(0.2, inplace=True),

            Noise(use_noise, sigma=noise_sigma),
            nn.Conv3d(ndf, ndf * 2, (1, 4, 4), stride=(1, 2, 2), padding=(0, 1, 1), bias=False),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Conv3d(ndf * 2, ndf * 2, (4, 1, 1), stride=1, padding=(1, 0, 0), bias=False),
            nn.BatchNorm3d(ndf * 2),
            nn.LeakyReLU(0.2, inplace=True),

            Noise(use_noise, sigma=noise_sigma),
            nn.Conv3d(ndf * 2, ndf * 4, (1, 4, 4), stride=(1, 2, 2), padding=(0, 1, 1), bias=False),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Conv3d(ndf * 4, ndf * 4, (4, 1, 1), stride=1, padding=(1, 0, 0), bias=False),
            nn.BatchNorm3d(ndf * 4),
            nn.LeakyReLU(0.2, inplace=True),

            nn.Conv3d(ndf * 4, ndf * 4, (1, 4, 4), stride=(1, 2, 2), padding=(0, 1, 1), bias=False),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Conv3d(ndf * 4, 1, (4, 1, 1), stride=1, padding=(1, 0, 0), bias=False)
        ])

    def forward(self, x):
        for layer in self.model:
            x = layer(x)
        return x, None
