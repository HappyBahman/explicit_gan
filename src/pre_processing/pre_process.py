"""
Copyright (C) 2017 NVIDIA Corporation.  All rights reserved.
Licensed under the CC BY-NC-ND 4.0 license (https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode).

Sergey Tulyakov, Ming-Yu Liu, Xiaodong Yang, Jan Kautz, MoCoGAN: Decomposing Motion and Content for Video Generation
https://arxiv.org/abs/1707.04993

Usage:
    pre_process.py [options]

Options:
    --vido_dataset=<path>           the folder containing all videos
    --destination_path=<path>       the destination folder for processed videos
    --sliding_step=<count>          step size for slicing videos [default: 8]
    --vid_len=<count>               num of frames in each video
    --size=<count>                  both height and width of frames
"""

import os
import docopt

from processing_utils import vid2folder, folder2image
from detect import mask_images

if __name__ == "__main__":
    args = docopt.docopt(__doc__)
    print(args)
    par_name = os.path.dirname(os.path.abspath(__file__))
    temp_frames_path = os.path.join(par_name, 'frames')
    os.makedirs(temp_frames_path, exist_ok=True)
    temp_maksed_path = os.path.join(par_name, 'maksed')
    os.makedirs(temp_maksed_path, exist_ok=True)
    # vid2folder(dataset_path=args['--vido_dataset'], destination_path=temp_frames_path)
    # mask_images(image_folder=temp_frames_path, output_folder=temp_maksed_path)
    folder2image(folder_path=temp_maksed_path, destination_path=args['--destination_path'],
                 sliding_step=int(args['--sliding_step']), vid_len=int(args['--vid_len']), size=int(args['--size']))
