from __future__ import division

from models import *
from utils.utils import *
from utils.datasets import *
from utils.augmentations import *
from utils.transforms import *

import os
import sys
import time
import datetime
import argparse

from PIL import Image

import torch
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
from torchvision import datasets
from torch.autograd import Variable

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.ticker import NullLocator


def mask_images(image_folder, output_folder, model_def="config/yolov3.cfg", weights_path="weights/yolov3.weights",
                class_path="data/coco.names", conf_thres=0.8, nms_thres=0.4, batch_size=1, n_cpu=0,
                img_size=416, mask_only=False):
    model_def = os.path.join(os.path.dirname(os.path.abspath(__file__)), model_def)
    weights_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), weights_path)
    class_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), class_path)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    os.makedirs(output_folder, exist_ok=True)

    # Set up model
    model = Darknet(model_def, img_size=img_size).to(device)

    if weights_path.endswith(".weights"):
        # Load darknet weights
        model.load_darknet_weights(weights_path)
    else:
        # Load checkpoint weights
        model.load_state_dict(torch.load(weights_path))

    model.eval()  # Set in evaluation mode

    classes = load_classes(class_path)  # Extracts class labels from file

    Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor

    imgs = []  # Stores image paths
    img_detections = []  # Stores detections for each image index

    print("\nPerforming object detection:")
    prev_time = time.time()
    videos = os.listdir(image_folder)
    for video in videos:
        video_folder = os.path.join(image_folder, video)
        dataloader = DataLoader(
            ImageFolder(video_folder, transform= \
                transforms.Compose([DEFAULT_TRANSFORMS, Resize(img_size)])),
            batch_size=batch_size,
            shuffle=False,
            num_workers=n_cpu,
        )
        for batch_i, (img_paths, input_imgs) in enumerate(dataloader):
            # Configure input
            input_imgs = Variable(input_imgs.type(Tensor))

            # Get detections
            with torch.no_grad():
                detections = model(input_imgs)
                detections = non_max_suppression(detections, conf_thres, nms_thres)

            # Log progress
            current_time = time.time()
            inference_time = datetime.timedelta(seconds=current_time - prev_time)
            prev_time = current_time
            print("\t+ Batch %d, Inference Time: %s" % (batch_i, inference_time))

            # Save image and detections
            # imgs.extend(img_paths)
            # img_detections.extend(detections)
            for img_i, (path, detections) in enumerate(zip(img_paths, detections)):
                norm_path = os.path.normpath(path)
                vid_name = norm_path.split(os.sep)[-2]

                # print("(%d) Image: '%s'" % (img_i, path))

                # Create plot
                img = np.array(Image.open(path))

                output_image = np.zeros(img.shape)

                # Draw bounding boxes and labels of detections
                if detections is not None:
                    # Rescale boxes to original image
                    detections = rescale_boxes(detections, img_size, img.shape[:2])
                    unique_labels = detections[:, -1].cpu().unique()
                    n_cls_preds = len(unique_labels)
                    # bbox_colors = random.sample(colors, n_cls_preds)
                    for x1, y1, x2, y2, conf, cls_conf, cls_pred in detections:
                        if classes[int(cls_pred)] != 'tvmonitor':
                            if not mask_only:
                                output_image[int(y1):int(y2), int(x1):int(x2), :] = img[int(y1):int(y2), int(x1):int(x2), :]
                            else:
                                output_image[int(y1):int(y2), int(x1):int(x2), :] = 1.0

                # Save generated image with detections
                plt.figure()
                fig, ax = plt.subplots(1)
                ax.imshow(output_image.astype(int))
                plt.axis("off")
                plt.gca().xaxis.set_major_locator(NullLocator())
                plt.gca().yaxis.set_major_locator(NullLocator())
                filename = os.path.basename(path).split(".")[0]
                output_path = os.path.join(output_folder, vid_name, filename + ".jpg")
                os.makedirs(os.path.join(output_folder, vid_name), exist_ok=True)
                plt.savefig(output_path, bbox_inches="tight", pad_inches=0.0)
                plt.close()



# print("\nSaving images:")
# # Iterate through images and save plot of detections
# for img_i, (path, detections) in enumerate(zip(imgs, img_detections)):
#     norm_path = os.path.normpath(path)
#     vid_name = norm_path.split(os.sep)[-2]
#
#     print("(%d) Image: '%s'" % (img_i, path))
#
#     # Create plot
#     img = np.array(Image.open(path))
#
#     output_image = np.zeros(img.shape)
#
#     # Draw bounding boxes and labels of detections
#     if detections is not None:
#         # Rescale boxes to original image
#         detections = rescale_boxes(detections, img_size, img.shape[:2])
#         unique_labels = detections[:, -1].cpu().unique()
#         n_cls_preds = len(unique_labels)
#         # bbox_colors = random.sample(colors, n_cls_preds)
#
#         for x1, y1, x2, y2, conf, cls_conf, cls_pred in detections:
#             output_image[x1:x2, y1:y2, :] = img[x1:x2, y1:y2, :]
#
#     # Save generated image with detections
#     plt.figure()
#     fig, ax = plt.subplots(1)
#     ax.imshow(output_image)
#     plt.axis("off")
#     plt.gca().xaxis.set_major_locator(NullLocator())
#     plt.gca().yaxis.set_major_locator(NullLocator())
#     filename = os.path.basename(path).split(".")[0]
#     output_path = os.path.join(output_folder, vid_name, f"{filename}.jpg")
#     os.makedirs(output_folder, exist_ok=True)
#     plt.savefig(output_path, bbox_inches="tight", pad_inches=0.0)
#     plt.close()
