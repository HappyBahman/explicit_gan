import os
import numpy as np
import cv2
from tqdm import tqdm


def vid2folder(dataset_path, destination_path):
    files = os.listdir(dataset_path)
    vid_cntr = 0
    for new_file in tqdm(files):
        vid_cntr += 1
        video_dest_path = os.path.join(destination_path, str(vid_cntr))
        os.makedirs(video_dest_path, exist_ok=True)
        cap = cv2.VideoCapture(os.path.join(dataset_path, new_file))
        frame_cntr = 0
        while (cap.isOpened()):
            ret, frame = cap.read()
            if ret:
                filename = os.path.join(video_dest_path, str(frame_cntr) + '.jpg')
                print(filename)
                stat=cv2.imwrite(filename, frame)
                print(stat)
                frame_cntr += 1
            else:
                break
        cap.release()


def vid2image():
    vid_len = 16
    categs = os.listdir('/content/UCF-101')
    for categ in tqdm(categs):
        new_dir = os.path.join('/content/UCF-101', categ)
        files = os.listdir(new_dir)
        for new_file in files:
            if new_file.split('.')[1] == 'jpg':
                break
            cap = cv2.VideoCapture(os.path.join(new_dir, new_file))
            frames = []
            frame_cntr = 0
            while (cap.isOpened()):
                ret, frame = cap.read()
                if ret:
                    # rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                    rgb_frame = frame
                    # resize image
                    dim = (64, 64)
                    resized = cv2.resize(frame, dim, interpolation=cv2.INTER_AREA)
                    frames.append(resized)
                    frame_cntr += 1
                    # if frame_cntr >=vid_len:
                    #   break
                else:
                    break
            cap.release()
            if len(frames) < vid_len:
                break
            if len(frames) > vid_len * 2:
                mid = len(frames) / 2
                start = int(np.floor(mid - vid_len / 2))
            else:
                start = 0
            mid_frames = frames[start: start + vid_len]
            new_video = cv2.hconcat(mid_frames)
            filename = new_dir + '/' + new_file.split('.')[0] + '.jpg'
            cv2.imwrite(filename, new_video)
            os.remove(os.path.join(new_dir, new_file))


# noinspection DuplicatedCode
def folder2image(folder_path, destination_path, sliding_step, vid_len=16, size=64):
    os.makedirs(destination_path, exist_ok=True)
    videos = os.listdir(folder_path)
    vid_cnt = 0
    for video in tqdm(videos):
        frames = []
        vid_cnt += 1
        video_path = os.path.join(folder_path, video)
        frame_paths = os.listdir(video_path)
        frame_paths = sorted(frame_paths)
        for frame_path in tqdm(frame_paths):
            frame = cv2.imread(os.path.join(video_path, frame_path))
            dim = (size, size)
            resized = cv2.resize(frame, dim, interpolation=cv2.INTER_AREA)
            frames.append(resized)
        if len(frames) < vid_len:
            break
        slice_cnt = 0
        for start in range(0, len(frames) - vid_len + 1, sliding_step):
            slice_cnt += 1
            sliced_frames = frames[start: start + vid_len]
            new_video = cv2.hconcat(sliced_frames)
            filename = os.path.join(destination_path, str(vid_cnt) + '_' + str(slice_cnt) + '.jpg')
            cv2.imwrite(filename, new_video)
