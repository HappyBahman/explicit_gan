"""
Copyright (C) 2017 NVIDIA Corporation.  All rights reserved.
Licensed under the CC BY-NC-ND 4.0 license (https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode).

Sergey Tulyakov, Ming-Yu Liu, Xiaodong Yang, Jan Kautz, MoCoGAN: Decomposing Motion and Content for Video Generation
https://arxiv.org/abs/1707.04993

Usage:
    train.py [options] <dataset> <log_folder>

Options:
    --image_dataset=<path>          specifies a separate dataset to train for images [default: ]
    --image_batch=<count>           number of images in image batch [default: 10]
    --video_batch=<count>           number of videos in video batch [default: 3]

    --image_size=<int>              resize all frames to this size [default: 64]

    --use_infogan                   when specified infogan loss is used

    --use_noise                     when specified instance noise is used
    --noise_sigma=<float>           when use_noise is specified, noise_sigma controls
                                    the magnitude of the noise [default: 0]

    --video_length=<len>            length of the video [default: 16]
    --print_every=<count>           print every iterations [default: 100]
    --save_every=<count>           print every iterations [default: 1000]
    --n_channels=<count>            number of channels in the input data [default: 3]
    --every_nth=<count>             sample training videos using every nth frame [default: 4]
    --batches=<count>               specify number of batches to train [default: 100000]

    --z_dim=<count>                 dimensionality of the latent space
    --ae_output_l_size=<count>      size for Autoencoders last layer size [default: 8]

    --load_from=<int>               the batch number to load data from [default: 0]
    --use_wasserstein               use wasserstein loss for training
"""

import os
import docopt
import PIL

import functools

import torch
from torch.utils.data import DataLoader
from torchvision import transforms

import models

from trainers import Trainer

import data

from util import count_parameters, summary

from torch.autograd import Variable
import numpy as np
from matplotlib import pyplot as plt
import math
from models import l_dim_vae, compute_mmd, exp_vae, PatchImageDiscriminator, FactorizedPatchVideoDiscriminator


def video_transform(video, image_transform):
    vid = []
    for im in video:
        vid.append(image_transform(im))

    vid = torch.stack(vid).permute(1, 0, 2, 3)

    return vid


if __name__ == "__main__":
    args = docopt.docopt(__doc__)
    print(args)

    n_channels = int(args['--n_channels'])

    image_transforms = transforms.Compose([
        PIL.Image.fromarray,
        transforms.Scale(int(args["--image_size"])),
        transforms.ToTensor(),
        lambda x: x[:n_channels, ::],
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
    ])

    video_transforms = functools.partial(video_transform, image_transform=image_transforms)

    video_length = int(args['--video_length'])
    image_batch = int(args['--image_batch'])
    video_batch = int(args['--video_batch'])

    dataset = data.VideoFolderDataset(args['<dataset>'], cache=os.path.join(args['<dataset>'], 'local.db'))
    # image_dataset = data.ImageDataset(dataset, image_transforms)
    # image_loader = DataLoader(image_dataset, batch_size=image_batch, drop_last=True, num_workers=2, shuffle=True)

    video_dataset = data.VideoDataset(dataset, 16, 2, video_transforms)
    video_loader = DataLoader(video_dataset, batch_size=video_batch, drop_last=True, num_workers=2, shuffle=True)

    ae_model = exp_vae(int(args['--z_dim']), int(args['--n_channels']),
                       int(args['--ae_output_l_size']), batch_size=video_batch)
    image_discriminator = PatchImageDiscriminator(3)
    video_discriminator = FactorizedPatchVideoDiscriminator(3)

    if torch.cuda.is_available():
        ae_model = ae_model.cuda()
        image_discriminator = image_discriminator.cuda()
        video_discriminator = video_discriminator.cuda()

    print('The number of parameters for AE is : {0}'.format(count_parameters(ae_model)))
    summary((n_channels, video_length, int(args["--image_size"]), int(args["--image_size"])), ae_model)

    trainer = Trainer(video_loader,
                      int(args['--print_every']),
                      int(args['--batches']),
                      args['<log_folder>'],
                      use_cuda=torch.cuda.is_available(),
                      use_infogan=args['--use_infogan'],
                      save_interval=int(args['--save_every']))

    trainer.train_xp_gan(ae_model, image_discriminator, video_discriminator,
                         int(args['--z_dim']), resume_from=int(args['--load_from']))
