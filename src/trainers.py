"""
Copyright (C) 2017 NVIDIA Corporation.  All rights reserved.
Licensed under the CC BY-NC-ND 4.0 license (https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode).
"""

import os
import time

import numpy as np

from logger import Logger
from torch.utils.tensorboard import SummaryWriter

import torch
from torch import nn
from torch.autograd import Variable
import torch.optim as optim
from models import l_dim_vae, compute_mmd

if torch.cuda.is_available():
    T = torch.cuda
else:
    T = torch


def images_to_numpy(tensor):
    generated = tensor.data.cpu().numpy().transpose(0, 2, 3, 1)
    generated[generated < -1] = -1
    generated[generated > 1] = 1
    generated = (generated + 1) / 2 * 255
    return generated.astype('uint8')


def videos_to_numpy(tensor):
    generated = tensor.data.cpu().numpy().transpose(0, 1, 2, 3, 4)
    generated[generated < -1] = -1
    generated[generated > 1] = 1
    generated = (generated + 1) / 2 * 255
    return generated.astype('uint8')


def one_hot_to_class(tensor):
    a, b = np.nonzero(tensor)
    return np.unique(b).astype(np.int32)


class Trainer(object):
    def __init__(self, video_sampler, log_interval, train_batches, log_folder, use_cuda=False,
                 use_infogan=True, use_categories=False, save_interval=None, psi = 0.5):

        self.use_categories = use_categories

        self.gan_criterion = nn.BCEWithLogitsLoss()
        self.category_criterion = nn.CrossEntropyLoss()

        # self.image_sampler = image_sampler
        self.video_sampler = video_sampler

        self.video_batch_size = self.video_sampler.batch_size
        # self.image_batch_size = self.image_sampler.batch_size

        self.log_interval = log_interval
        if save_interval is None:
            self.save_interval = log_interval
        else:
            self.save_interval = save_interval
        self.train_batches = train_batches

        self.log_folder = log_folder

        self.use_cuda = use_cuda
        self.use_infogan = use_infogan

        self.image_enumerator = None
        self.video_enumerator = None
        self.psi = psi


    @staticmethod
    def ones_like(tensor, val=1.):
        return Variable(T.FloatTensor(tensor.size()).fill_(val), requires_grad=False)

    @staticmethod
    def zeros_like(tensor, val=0.):
        return Variable(T.FloatTensor(tensor.size()).fill_(val), requires_grad=False)

    def compute_gan_loss(self, discriminator, sample_true, sample_fake, is_video):
        real_batch = sample_true()

        batch_size = real_batch['images'].size(int(0))
        fake_batch, generated_categories = sample_fake(batch_size)

        real_labels, real_categorical = discriminator(Variable(real_batch['images']))
        fake_labels, fake_categorical = discriminator(fake_batch)

        fake_gt, real_gt = self.get_gt_for_discriminator(batch_size, real=0.)

        l_discriminator = self.gan_criterion(real_labels, real_gt) + \
                          self.gan_criterion(fake_labels, fake_gt)

        #  sample again and compute for generator

        fake_gt = self.get_gt_for_generator(batch_size)
        # to real_gt
        l_generator = self.gan_criterion(fake_labels, fake_gt)

        if is_video:

            # Ask the video discriminator to learn categories from training videos
            categories_gt = Variable(torch.squeeze(real_batch['categories'].long()))
            l_discriminator += self.category_criterion(real_categorical, categories_gt)

            if self.use_infogan:
                # Ask the generator to generate categories recognizable by the discriminator
                l_generator += self.category_criterion(fake_categorical, generated_categories)

        return l_generator, l_discriminator

    def sample_real_image_batch(self):
        if self.image_enumerator is None:
            self.image_enumerator = enumerate(self.image_sampler)

        batch_idx, batch = next(self.image_enumerator)
        b = batch
        if self.use_cuda:
            for k, v in batch.items():
                b[k] = v.cuda()

        if batch_idx == len(self.image_sampler) - 1:
            self.image_enumerator = enumerate(self.image_sampler)

        # return b

    def sample_real_video_batch(self):
        if self.video_enumerator is None:
            self.video_enumerator = enumerate(self.video_sampler)

        batch_idx, batch = next(self.video_enumerator)
        b = batch
        if self.use_cuda:
            for k, v in batch.items():
                b[k] = v.cuda()

        if batch_idx == len(self.video_sampler) - 1:
            self.video_enumerator = enumerate(self.video_sampler)

        return b

    def compute_reconstruction_loss_with_mask(self, x, x_reconstructed, true_samples, z):
        psi = 10
        # with torch.no_grad():
        #     mask = torch.round(x)
        # x_reconstructed = x_reconstructed * mask
        mmd = compute_mmd(true_samples, z)
        nll = (x_reconstructed - x).pow(2).mean()
        loss = nll * psi + mmd
        return loss, nll, mmd

    def compute_reconstruction_loss(self, x, x_reconstructed, true_samples, z):
        mmd = compute_mmd(true_samples, z)
        nll = (x_reconstructed - x).pow(2).mean()
        loss = nll + mmd
        return loss, nll

    def mask_image(self, x):
        with torch.no_grad():
            x_max = torch.max(x, dim = 1).values
            mask_t = torch.ceil(( x_max + 1)/2)
            mask = mask_t.unsqueeze(1).expand(x.size()[0], x.size()[1], -1, -1,  -1)
        return mask

    def train_ae_w_mask(self, ae, opt, true_samples):
        opt.zero_grad()
        # train on images
        x = self.sample_real_video_batch()
        x = Variable(x['images'], requires_grad=False)
        x = self.mask_image(x)
        z, x_reconstructed, _ = ae(x, masked=True)
        l_ae, nll, mmd = self.compute_reconstruction_loss_with_mask(x, x_reconstructed, true_samples, z)
        # train on videos
        l_ae.backward()
        opt.step()
        return l_ae, nll, mmd

    def train_ae(self, ae, opt, true_samples):
        opt.zero_grad()
        # train on images
        x = self.sample_real_video_batch()
        x = Variable(x['images'], requires_grad=False)
        x_masked = self.mask_image(x)
        z, x_reconstructed, _ = ae(x_masked, x_original=x)
        l_ae, nll, mmd = self.compute_reconstruction_loss_with_mask(x, x_reconstructed, true_samples, z)
        # train on videos
        l_ae.backward()
        opt.step()
        return l_ae, nll, mmd

    def train(self, ae, z_dim, resume_from=0):
        models = [ae]
        if resume_from != 0:
            batch_num = int(resume_from)
            ae = torch.load(os.path.join(self.log_folder, 'ae_%05d.pytorch' % batch_num))
        else:
            batch_num = 0

        if self.use_cuda:
            for model in models:
                model.cuda()

        if resume_from != 0:
            for model in models:
                model.eval()

        logger = Logger(self.log_folder)
        writer = SummaryWriter(self.log_folder)

        optimizer = torch.optim.Adam(model.parameters(), lr=1e-4)
        optimizers = [optimizer]

        def init_logs():
            return {'l_ae': 0, 'nll': 0, 'mmd': 0}

        logs = init_logs()

        start_time = time.time()

        while True:
            for model in models:
                model.train()
            for opt in optimizers:
                opt.zero_grad()

            # train image discriminator
            true_samples = Variable(torch.randn(self.video_batch_size , z_dim).cuda(), requires_grad=False)
            l_ae, nll, mmd = self.train_ae(ae, optimizer, true_samples)

            logs['l_ae'] += l_ae.data
            logs['nll'] += nll.data
            logs['mmd'] += mmd.data

            batch_num += 1

            if batch_num % self.log_interval == 0:

                log_string = "Batch %d" % batch_num
                for k, v in logs.items():
                    log_string += " [%s] %5.3f" % (k, v / self.log_interval)

                log_string += ". Took %5.2f" % (time.time() - start_time)

                print(log_string)

                for tag, value in logs.items():
                    logger.scalar_summary(tag, value / self.log_interval, batch_num)
                    writer.add_scalar(tag, value / self.log_interval, batch_num)

                logs = init_logs()
                start_time = time.time()

                ae.eval()

                log_batch = self.sample_real_video_batch()
                log_batch_v = Variable(log_batch['images'], requires_grad=False)
                z, reconstructed_log, mask = ae(log_batch_v)
                logger.video_summary("Videos", videos_to_numpy(log_batch_v), batch_num)
                # writer.add_image('Videos', log_batch_v)
                logger.video_summary("Videos_reconstructed", videos_to_numpy(reconstructed_log), batch_num)
                # writer.add_image('Videos_reconstructed', reconstructed_log)
                logger.video_summary("mask", videos_to_numpy(mask), batch_num)
                writer.add_histogram('z', z, batch_num)


            if batch_num % self.save_interval == 0:
                torch.save(ae, os.path.join(self.log_folder, 'ae_%05d.pytorch' % batch_num))

            if batch_num >= self.train_batches:
                torch.save(ae, os.path.join(self.log_folder, 'ae_%05d.pytorch' % batch_num))
                break

    def train_discriminator(self, discriminator, sample_true, sample_fake, opt, batch_size):
        opt.zero_grad()

        real_batch = sample_true()
        batch = Variable(real_batch['images'], requires_grad=False)

        # util.show_batch(batch.data)

        fake_batch, generated_categories = sample_fake(batch_size)

        real_labels, real_categorical = discriminator(batch)
        fake_labels, fake_categorical = discriminator(fake_batch.detach())

        ones = self.ones_like(real_labels)
        zeros = self.zeros_like(fake_labels)

        l_discriminator = self.gan_criterion(real_labels, ones) + \
                          self.gan_criterion(fake_labels, zeros)

        l_discriminator.backward()
        opt.step()

        return l_discriminator

    def train_generator(self, image_discriminator, video_discriminator, sample_fake_images, sample_fake_videos, opt):
        opt.zero_grad()
        # # train on images
        # fake_batch, generated_categories = sample_fake_images(self.image_batch_size)
        # fake_labels, fake_categorical = image_discriminator(fake_batch)
        # all_ones = self.ones_like(fake_labels)
        #
        # l_generator = self.gan_criterion(fake_labels, all_ones)

        # train on videos
        fake_batch, masked_batch = sample_fake_videos(self.video_batch_size)
        fake_labels, fake_categorical = video_discriminator(fake_batch)
        all_ones = self.ones_like(fake_labels)

        l_generator = self.gan_criterion(fake_labels, all_ones)
        l_location_consistency = (self.mask_image(fake_batch) - masked_batch).pow(2).mean()
        l_generator += l_location_consistency * self.psi
        l_generator.backward()
        opt.step()

        return l_generator

    def train_xp_gan(self, ae, image_discriminator, video_discriminator,
                     z_dim, resume_from=0):

        # todo add image disciminator
        models = [ae, image_discriminator, video_discriminator]
        if resume_from != 0:
            batch_num = int(resume_from)
            ae = torch.load(
                os.path.join(self.log_folder, 'ae_%05d.pytorch' % batch_num))
            image_discriminator = torch.load(
                os.path.join(self.log_folder, 'im_disc_%05d.pytorch' % batch_num))
            video_discriminator = torch.load(
                os.path.join(self.log_folder, 'vid_disc_%05d.pytorch' % batch_num))
        else:
            batch_num = 0

        if self.use_cuda:
            for model in models:
                model.cuda()

        if resume_from != 0:
            for model in models:
                model.eval()

        logger = Logger(self.log_folder)
        writer = SummaryWriter(self.log_folder)

        opt_ae_masked = torch.optim.Adam(ae.parameters(), lr=1e-4)
        opt_ae = torch.optim.Adam(ae.parameters(), lr=1e-4)
        opt_generator = torch.optim.Adam(ae.parameters(), lr=1e-4)
        # opt_image_discriminator = torch.optim.Adam(image_discriminator.parameters(),
        #     lr=1e-4, betas=(0.5, 0.999), weight_decay=0.00001)
        opt_video_discriminator = torch.optim.Adam(video_discriminator.parameters(),
            lr=1e-4, betas=(0.5, 0.999), weight_decay=0.00001)

        optimizers = [opt_ae_masked, opt_ae, opt_video_discriminator, opt_generator]

        def init_logs():
            return {'l_image_disc' : 0, 'l_video_dis' : 0,
             'l_ae': 0, 'l_gen':0, 'nll': 0, 'mmd': 0,
             'l_ae_masked': 0, 'nll_masked': 0, 'mmd_masked': 0}

        # todo add method to ae
        # def sample_fake_image_batch(batch_size):
        #     return ae.sample_images(batch_size)

        def sample_fake_video_batch(batch_size):
            x = self.sample_real_video_batch()
            x = Variable(x['images'], requires_grad=False)
            x = self.mask_image(x)
            _, x_reconstructed, _ = ae(x, masked=False, GAN=True)
            return x_reconstructed, x

        logs = init_logs()

        start_time = time.time()

        while True:
            for model in models:
                model.train()
            for opt in optimizers:
                opt.zero_grad()

            # # train AE with mask
            samples = Variable(torch.randn(self.video_batch_size, z_dim).cuda(), requires_grad=False)
            l_ae_masked, nll_masked, mmd_masked = self.train_ae_w_mask(ae, opt_ae, samples)

            # train AE
            samples = Variable(torch.randn(self.video_batch_size, z_dim).cuda(), requires_grad=False)
            l_ae, nll, mmd = self.train_ae(ae, opt_ae, samples)
            # train Image Discriminator
            # l_image_dis = self.train_discriminator(image_discriminator,
            #     self.sample_real_image_batch, sample_fake_image_batch,
            #     opt_image_discriminator, self.image_batch_size, use_categories=False)

            # train video discriminator
            l_video_dis = self.train_discriminator(video_discriminator,
                self.sample_real_video_batch, sample_fake_video_batch,
                opt_video_discriminator, self.video_batch_size)

            # train generator
            l_gen = self.train_generator(image_discriminator, video_discriminator,
                                         None, sample_fake_video_batch, opt_generator)
            # log the losses
            logs['l_ae'] += l_ae.data
            logs['nll'] += nll.data
            logs['mmd'] += mmd.data
            logs['l_ae_masked'] += l_ae_masked.data
            logs['nll_masked'] += nll_masked.data
            logs['mmd_masked'] += mmd_masked.data
            # logs['l_image_dis'] += l_image_dis.data
            logs['l_video_dis'] += l_video_dis.data
            logs['l_gen'] += l_gen.data

            batch_num += 1
            if batch_num % self.log_interval == 0:

                log_string = "Batch %d" % batch_num
                for k, v in logs.items():
                    log_string += " [%s] %5.3f" % (k, v / self.log_interval)

                log_string += ". Took %5.2f" % (time.time() - start_time)

                print(log_string)

                for tag, value in logs.items():
                    logger.scalar_summary(tag, value / self.log_interval, batch_num)
                    writer.add_scalar(tag, value / self.log_interval, batch_num)

                logs = init_logs()
                start_time = time.time()

                ae.eval()

                log_batch = self.sample_real_video_batch()
                log_batch_v = Variable(log_batch['images'], requires_grad=False)
                z, reconstructed_log, mask = ae(log_batch_v)
                logger.video_summary("Videos", videos_to_numpy(log_batch_v), batch_num)
                # writer.add_image('Videos', log_batch_v)
                logger.video_summary("Videos_reconstructed", videos_to_numpy(reconstructed_log), batch_num)
                # writer.add_image('Videos_reconstructed', reconstructed_log)
                logger.video_summary("mask", videos_to_numpy(mask), batch_num)
                writer.add_histogram('z', z, batch_num)
                generated_videos = self.sample_fake_videos(self.video_batch_size)
                logger.video_summary("Videos_generated", videos_to_numpy(generated_videos), batch_num)
                # writer.add_image('Videos_reconstructed', reconstructed_log)


            if batch_num % self.save_interval == 0:
                torch.save(ae, os.path.join(self.log_folder, 'ae_%05d.pytorch' % batch_num))
                torch.save(image_discriminator, os.path.join(self.log_folder, 'im_disc_%05d.pytorch' % batch_num))
                torch.save(video_discriminator, os.path.join(self.log_folder, 'vid_disc_%05d.pytorch' % batch_num))

            if batch_num >= self.train_batches:
                torch.save(ae, os.path.join(self.log_folder, 'ae_%05d.pytorch' % batch_num))
                torch.save(image_discriminator, os.path.join(self.log_folder, 'im_disc_%05d.pytorch' % batch_num))
                torch.save(video_discriminator, os.path.join(self.log_folder, 'vid_disc_%05d.pytorch' % batch_num))
                break
